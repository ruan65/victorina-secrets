package com.engstuff.quizwebpanel.data.repositories;

import com.engstuff.quizwebpanel.entities.QuizGuessComposer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface QuizGuessComposerRepo
        extends MongoRepository<QuizGuessComposer, String> {

        @Query("{ '_id' : ?0 }")
        QuizGuessComposer findById(String _id);

        List<QuizGuessComposer> findByCreatedBy(String name);
}
