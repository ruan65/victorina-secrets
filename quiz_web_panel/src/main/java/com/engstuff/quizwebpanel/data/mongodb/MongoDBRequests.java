package com.engstuff.quizwebpanel.data.mongodb;

import com.engstuff.quizwebpanel.entities.Author;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class MongoDBRequests {

    private final static String mongoInstance = "95.85.17.118:55065";
    private final static String usersDb = "clients";
    private final static String classicalDb = "classical";
    private final static String credColl = "classical_cred";
    private final static String localeColl = "locale";
    private final static String authorColl = "author";
    private final static String melodyColl = "melody";


    private static DBCollection createCollection(String dbInstance, String dbName, String cName)
            throws UnknownHostException {
        return new MongoClient(dbInstance).getDB(dbName).getCollection(cName);
    }

    public static DBCursor getCredColl() throws UnknownHostException {

        return createCollection(mongoInstance, usersDb, credColl).find(new BasicDBObject());
    }

    public static List<String> getLocales() throws UnknownHostException {

        DBCursor dbObjects = createCollection(mongoInstance, classicalDb, localeColl).find();

        return (List<String>) dbObjects.next().get("locale");
    }

    public static List<String> getAllAuthorsShortName() throws UnknownHostException {

        List<String> authors = new ArrayList<>();

        createCollection(mongoInstance, classicalDb, authorColl)
                .find(new BasicDBObject(), new BasicDBObject("_id", 1))
             // Above I found all authors short names like this {_id: "Mozart", _id: "Bethoven", ...}
                .forEach((author) -> authors.add(
                        (String) author.get("_id")
                ));

        Collections.sort(authors);

        return authors;
    }
}
