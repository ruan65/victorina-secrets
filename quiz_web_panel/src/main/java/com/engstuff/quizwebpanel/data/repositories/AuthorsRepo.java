package com.engstuff.quizwebpanel.data.repositories;

import com.engstuff.quizwebpanel.entities.Author;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface AuthorsRepo extends MongoRepository<Author, String> {

    Author findByNameShort(String name);
}
