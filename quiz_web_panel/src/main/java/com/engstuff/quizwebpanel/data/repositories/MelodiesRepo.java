package com.engstuff.quizwebpanel.data.repositories;

import com.engstuff.quizwebpanel.entities.Melody;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MelodiesRepo extends MongoRepository<Melody, String> {

    @Query("{ '_id' : ?0 }")
    Melody findById(String _id);

    List<Melody> findByAuthorNameShort(String nameShort);

    List<Melody> findByDocumentCreator(String name);

}
