package com.engstuff.quizwebpanel.data;

public enum DifficultyLevel {

    VERY_EASY, EASY, MEDIUM, HARD, VERY_HARD

}
