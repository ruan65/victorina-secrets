package com.engstuff.quizwebpanel.entities;

import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class QuestionGuessComposer implements Serializable{

    @Id
    private String _id;

    private String tag;

    private String theQuestion;

    private Melody melody;

    private String givenAnswer;

    private Set<String> answersSet = new HashSet<>();

    private Map<String, Map> localization;

    public QuestionGuessComposer() {
        localization = new HashMap<>();
        localization.put("en", null);
    }

    public QuestionGuessComposer(Melody melody) {
        this();
        this.melody = melody;
    }

    public void localize(String locale,
                         String theQuestion,
                         Set<String> answersSet) {
        Map map = new HashMap<>();
        map.put("theQuestion", theQuestion);
        map.put("answersSet", answersSet);
        localization.put(locale, map);
    }

    public void addAnswer(String answer) {
        answersSet.add(answer);
    }

    public boolean isGivenAnswerCorrect() {
        return givenAnswer.equalsIgnoreCase(melody.getAuthor().getNameShort());
    }

    public boolean isSetIncludeRightAnswer() {
        return answersSet.contains(melody.getAuthor().getNameShort());
    }

    public Melody getMelody() {
        return melody;
    }

    public void setMelody(Melody melody) {
        this.melody = melody;
    }

    public String getGivenAnswer() {
        return givenAnswer;
    }

    public void setGivenAnswer(String givenAnswer) {
        this.givenAnswer = givenAnswer;
    }

    public Set<String> getAnswersSet() {
        return answersSet;
    }

    public void setAnswersSet(Set<String> answersSet) {
        this.answersSet = answersSet;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTheQuestion() {
        return theQuestion;
    }

    public void setTheQuestion(String theQuestion) {
        this.theQuestion = theQuestion;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
