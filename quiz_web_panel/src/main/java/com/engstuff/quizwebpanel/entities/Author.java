package com.engstuff.quizwebpanel.entities;

import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Author implements Serializable {

    @Id
    private String nameShort;

    private String nameFull;
    private String country;
    private String infoUrl;

    private int birthYear;

    private Map<String, Map<String, String>> localisation;

    private String documentCreator;
    private String lastChangedBy;

    private Date createdTimeStamp;
    private Date lastChangedTimeStamp;

    public Author() {
        localisation = new HashMap<>();
        localisation.put("en", null);
    }

    public void localize(String locale,
                         String nameShort,
                         String nameFull,
                         String country,
                         String infoUrl,
                         String localizedBy) {

        Map map = new HashMap<>();
        map.put("nameShort", nameShort);
        map.put("nameFull", nameFull);
        map.put("country", country);
        map.put("infoUrl", infoUrl);
        map.put("localizedBy", localizedBy);
        map.put("localizedTimeStamp", new Date());
        localisation.put(locale, map);
    }

    public String getNameShort() {
        return nameShort;
    }

    public void setNameShort(String nameShort) {
        this.nameShort = nameShort;
    }

    public String getNameFull() {
        return nameFull;
    }

    public void setNameFull(String nameFull) {
        this.nameFull = nameFull;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public Map<String, Map<String, String>> getLocalisation() {
        return localisation;
    }

    public void setLocalisation(Map<String, Map<String, String>> localisation) {
        this.localisation = localisation;
    }

    public String getLastChangedBy() {
        return lastChangedBy;
    }

    public void setLastChangedBy(String lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
    }

    public Date getLastChangedTimeStamp() {
        return lastChangedTimeStamp;
    }

    public void setLastChangedTimeStamp(Date lastChangedTimeStamp) {
        this.lastChangedTimeStamp = lastChangedTimeStamp;
    }

    public String getDocumentCreator() {
        return documentCreator;
    }

    public void setDocumentCreator(String documentCreator) {
        this.documentCreator = documentCreator;
    }

    public Date getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    public void setCreatedTimeStamp(Date createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }
}
