package com.engstuff.quizwebpanel.entities;

import com.engstuff.quizwebpanel.data.DifficultyLevel;

import java.util.*;

public class QuizGuessComposer {

    private String _id;
    private String title;

    private boolean productionReady;

    private DifficultyLevel difficultyLevel;
    private int size;

    private String createdBy;
    private Date createdTimeStamp;

    private String lastModifiedBy;
    private Date lastModifiedTimeStamp;

    private int numberCorrectAnswers;

    private List<QuestionGuessComposer> questions = new ArrayList<>();

    private Map<String, Map<String, String>> localisation;

    public QuizGuessComposer() {
        localisation = new HashMap<>();
        localisation.put("en", null);
        createdTimeStamp = new Date();
    }

    public QuizGuessComposer(String pruducer, String title, int size, DifficultyLevel difficultyLevel) {
        this();
        this.createdBy = pruducer;
        this.title = title;
        this.size = size;
        this.difficultyLevel = difficultyLevel;
    }

    public void localize(String locale,
                         String title,
                         String localizedBy) {

        Map map = new HashMap<>();
        map.put("title", title);
        map.put("localizedBy", localizedBy);
        map.put("localizedTimeStamp", new Date());
        localisation.put(locale, map);
    }

    public void addQuestion(QuestionGuessComposer q) {
        questions.add(q);
    }
    public void addCorrectAnswer() {
        numberCorrectAnswers++;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public DifficultyLevel getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    public void setCreatedTimeStamp(Date createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedTimeStamp() {
        return lastModifiedTimeStamp;
    }

    public void setLastModifiedTimeStamp(Date lastModifiedTimeStamp) {
        this.lastModifiedTimeStamp = lastModifiedTimeStamp;
    }

    public int getNumberCorrectAnswers() {
        return numberCorrectAnswers;
    }

    public void setNumberCorrectAnswers(int numberCorrectAnswers) {
        this.numberCorrectAnswers = numberCorrectAnswers;
    }

    public List<QuestionGuessComposer> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionGuessComposer> questions) {
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Map<String, String>> getLocalisation() {
        return localisation;
    }

    public void setLocalisation(Map<String, Map<String, String>> localisation) {
        this.localisation = localisation;
    }

    public boolean isProductionReady() {
        return productionReady;
    }

    public void setProductionReady(boolean productionReady) {
        this.productionReady = productionReady;
    }
}
