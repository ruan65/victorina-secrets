package com.engstuff.quizwebpanel.entities;

import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Melody implements Serializable{

    @Id
    private String _id;


    private String name;
    private String infoUrl;
    private String performer;

    private Author author;

    private int year;

    private Map<String, Map<String, String>> localisation;

    private String documentCreator;
    private String lastChangedBy;

    private Date createdTimeStamp;
    private Date lastChangedTimeStamp;

    public Melody() {
        localisation = new HashMap<>();
        localisation.put("en", null);
    }

    public void localize(String locale,
                         String name,
                         String performer,
                         String infoUrl,
                         String localizedBy) {

        Map map = new HashMap<>();
        map.put("name", name);
        map.put("performer", performer);
        map.put("infoUrl", infoUrl);
        map.put("localizedBy", localizedBy);
        map.put("localizedTimeStamp", new Date());
        localisation.put(locale, map);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Map<String, Map<String, String>> getLocalisation() {
        return localisation;
    }

    public void setLocalisation(Map<String, Map<String, String>> localisation) {
        this.localisation = localisation;
    }

    public String getLastChangedBy() {
        return lastChangedBy;
    }

    public void setLastChangedBy(String lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
    }

    public Date getLastChangedTimeStamp() {
        return lastChangedTimeStamp;
    }

    public void setLastChangedTimeStamp(Date lastChangedTimeStamp) {
        this.lastChangedTimeStamp = lastChangedTimeStamp;
    }

    public String getDocumentCreator() {
        return documentCreator;
    }

    public void setDocumentCreator(String documentCreator) {
        this.documentCreator = documentCreator;
    }

    public Date getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    public void setCreatedTimeStamp(Date createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }
}
