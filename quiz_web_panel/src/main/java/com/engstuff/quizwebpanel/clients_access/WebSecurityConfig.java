package com.engstuff.quizwebpanel.clients_access;

import com.mongodb.Cursor;
import com.mongodb.DBObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.engstuff.quizwebpanel.data.mongodb.MongoDBRequests.*;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {

//        CharacterEncodingFilter filter = new CharacterEncodingFilter();
//        filter.setEncoding("UTF-8");
//        filter.setForceEncoding(true);
//        http.addFilterBefore(filter, CsrfFilter.class);

        http.authorizeRequests()

//                .antMatchers("/", "main").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .csrf().disable();

        http.formLogin()

                .loginPage("/login").permitAll();


    }


    @Configuration
    protected static class AuthConfig extends GlobalAuthenticationConfigurerAdapter {

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {

            Cursor c = getCredColl();
            try {
                while (c.hasNext()) {
                    DBObject o = c.next();

                    auth
                            .inMemoryAuthentication()
                            .withUser(o.get("_id").toString())
                            .password(o.get("p").toString())
                            .roles(o.get("role").toString());
                }
            } catch (Exception e) {
            } finally {
                c.close();
            }
        }
    }
}
