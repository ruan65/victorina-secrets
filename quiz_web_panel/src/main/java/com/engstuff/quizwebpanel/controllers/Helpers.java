package com.engstuff.quizwebpanel.controllers;

import org.springframework.ui.Model;

public final class Helpers {

    public static String handleDbConnectionError(Model m, Exception e, String message) {
        System.err.println(e.getMessage() + "\n" + e);
        m.addAttribute("error", message + ", DB is inaccessible, try later or call DBA");
        return "errors_page";
    }
}
