package com.engstuff.quizwebpanel.controllers;

import com.engstuff.quizwebpanel.data.mongodb.MongoDBRequests;
import com.engstuff.quizwebpanel.data.repositories.AuthorsRepo;
import com.engstuff.quizwebpanel.data.repositories.MelodiesRepo;
import com.engstuff.quizwebpanel.entities.Author;
import com.engstuff.quizwebpanel.entities.Melody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.Date;
import java.util.Map;

import static com.engstuff.quizwebpanel.controllers.Helpers.handleDbConnectionError;

@Controller
public class DataMelodiesController {

    @Autowired
    MelodiesRepo melodiesRepo;

    @Autowired
    AuthorsRepo authorsRepo;

    /**
     * List of all melodies
     */
    @RequestMapping("/melodies")
    public String allMelodies(Model m) {

        try {
            m.addAttribute("melodies", melodiesRepo.findAll());
            return "melodies/melodies";
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get melodies");
        }
    }

    /**
     * Detailed info about melody (localized version included)
     */
    @RequestMapping("/melody/{locale}")
    public String melody(Model m
            , @RequestParam("_id") String _id
            , @PathVariable("locale") String locale) {

        try {
            Melody melody = melodiesRepo.findById(_id);
            if (melody == null) {
                m.addAttribute("error", "Can't get melody from DB");
                return "errors_page";
            }
            if (!locale.equals("en")) {

                Map<String, String> localeMap = melody.getLocalisation().get(locale);

                m.addAttribute("localName", localeMap.get("name"));
                m.addAttribute("performer", localeMap.get("performer"));
                m.addAttribute("infoUrl", localeMap.get("infoUrl"));
                Map<String, String> authorLocale = melody
                        .getAuthor()
                        .getLocalisation()
                        .get(locale);
                if (authorLocale == null) {
                    m.addAttribute("error", "Author not localized this locale:\n" +
                            "1. Localize Author\n2. Update Melody\n3. Then do localization");
                    return "errors_page";
                }
                m.addAttribute("authorLocal", authorLocale.get("nameFull"));

            }
            m.addAttribute("melody", melody);
            m.addAttribute("locales", melody.getLocalisation().keySet());
            m.addAttribute("this_locale", locale);

            return "melodies/details_melody";

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get melody info");
        }
    }

    private void saveFileOnHdd(MultipartFile file, String name) throws IOException {
        byte[] bytes = file.getBytes();
        BufferedOutputStream stream =
                new BufferedOutputStream(new FileOutputStream(new File("/opt/classical/" + name)));
        stream.write(bytes);
        stream.close();
    }

    /**
     * This 2 handlers supports as adding melody as also updating it
     */
    @RequestMapping("/create/melody")
    public String createMelody(Model m,
                               @RequestParam(value = "_id", required = false) String _id) {
        try {
            m.addAttribute("authors", MongoDBRequests.getAllAuthorsShortName());
            m.addAttribute("melody", _id == null ? new Melody() : melodiesRepo.findById(_id));

            return "melodies/create_melody";

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't connect to the DB");
        }
    }

    @RequestMapping(value = "/create/melody", method = RequestMethod.POST)
    public String saveMelody(Model m, Principal p,
                             @ModelAttribute Melody melody,
                             @RequestParam(value = "_id", required = false) String _id,
                             @RequestParam("file") MultipartFile file,
                             @RequestParam("author_n") String author_n) {

        if (!file.isEmpty()) {
            try {
                Author author = authorsRepo.findByNameShort(author_n);
                melody.setAuthor(author);
                melody.setLastChangedBy(p.getName());
                melody.setLastChangedTimeStamp(new Date());

                if (_id != null) {
                    melody.set_id(_id);
                    // if melody has localization I need to keep it
                    Melody existedMelody = melodiesRepo.findById(_id);
                    melody.setLocalisation(existedMelody.getLocalisation());
                    melody.setDocumentCreator(existedMelody.getDocumentCreator());
                    melody.setCreatedTimeStamp(existedMelody.getCreatedTimeStamp());
                } else {
                    // if melody created first time (has no _id)
                    melody.setCreatedTimeStamp(new Date());
                    melody.setDocumentCreator(p.getName());
                }

                melody = melodiesRepo.save(melody);

                saveFileOnHdd(file, melody.get_id());

            } catch (Exception e) {
                e.printStackTrace();
                return handleDbConnectionError(m, e, "Failed to create melody --> error: " + e.getMessage());
            }
        }
        return "redirect:/melody/en?_id=" + melody.get_id();
    }

    /**
     * This handlers for the Melody localization
     */

    @RequestMapping("/create/melody/localize/author")
    public String chooseLocale(@RequestParam(value = "_id") String _id, Model m) {


        try {
            m.addAttribute("melody", melodiesRepo.findById(_id));
            m.addAttribute("locales", MongoDBRequests.getLocales());
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get Melody from DB: " + e);
        }

        return "melodies/localize_melody_check_author";
    }

    @RequestMapping("/create/melody/localize/{_id}")
    public String checkIfAuthorLocalized(Model m,
                                         @PathVariable("_id") String _id,
                                         @RequestParam("author_name") String author_name,
                                         @RequestParam("locale") String locale) {


        try {
            Author author = authorsRepo.findByNameShort(author_name);
            if (!author.getLocalisation().containsKey(locale)) {
                m.addAttribute("error", "Author not localized this locale: " + locale +
                        "\n1. Localize Author\n2. Do melody localization");
                return "errors_page";
            }
            Melody melody = melodiesRepo.findById(_id);

            String name_full = melody.getAuthor().getLocalisation().get(locale).get("nameFull");

            m.addAttribute("author_local_name", name_full);
            m.addAttribute("this_locale", locale);
            m.addAttribute("melody", melody);

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get Author of melody from DB: " + e);
        }

        return "melodies/localize_melody";
    }

    @RequestMapping(value = "/create/melody/localize/{_id}", method = RequestMethod.POST)
    public String saveLocalizedMelody(Model m, Principal p
            , @PathVariable("_id") String _id
            , @RequestParam("locale") String locale
            , @RequestParam("name") String name
            , @RequestParam("performer") String performer
            , @RequestParam("infoUrl") String infoUrl
    ) {

        Melody melody;

        try {
            melody = melodiesRepo.findById(_id);
            melody.setLastChangedBy(p.getName());
            melody.setLastChangedTimeStamp(new Date());
            melody.localize(locale, name, performer, infoUrl, p.getName());
            melody = melodiesRepo.save(melody);

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't localize melody");
        }

        if (melody != null) {
            m.addAttribute("melody", melody);
            return "redirect:/melody/" + locale + "?_id=" + melody.get_id();
        }
        m.addAttribute("error", "Can't localize melody");
        return "errors_page";
    }

    /**
     * This handlers for removing the Melody from DB
     */
    @RequestMapping(value = "/delete/melody/{_id}", method = RequestMethod.POST)
    public String deleteMelody(@PathVariable("_id") String _id, Model m) {
        try {
            Melody melody = melodiesRepo.findById(_id);
            if (melody != null) {
                melodiesRepo.delete(melody);
                return "redirect:/melodies";
            }
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't delete melody");
        }
        m.addAttribute("error", "Can't delete melody");
        return "errors_page";
    }
}
