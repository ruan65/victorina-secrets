package com.engstuff.quizwebpanel.controllers;

import com.engstuff.quizwebpanel.clients_access.WebSecurityConfig;
import com.engstuff.quizwebpanel.data.mongodb.MongoDBRequests;
import com.engstuff.quizwebpanel.data.repositories.AuthorsRepo;
import com.engstuff.quizwebpanel.data.repositories.MelodiesRepo;
import com.engstuff.quizwebpanel.entities.Author;
import com.engstuff.quizwebpanel.entities.Melody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.engstuff.quizwebpanel.controllers.Helpers.handleDbConnectionError;

@Controller
public class DataAuthorsController {

    @Autowired
    AuthorsRepo authorsRepo;

    @Autowired
    MelodiesRepo melodiesRepo;

    /**
     * List of all authors
     */
    @RequestMapping("/authors")
    public String allAuthors(Model m) {

        try {
            m.addAttribute("authors", authorsRepo.findAll());
            return "authors/authors";
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get authors");
        }
    }

    /**
     * Detailed info about author (localized version included)
     */
    @RequestMapping("/author/{locale}")
    public String author(
              Model m
            , @RequestParam("name") String name
            , @PathVariable("locale") String locale) {

        try {
            Author author = authorsRepo.findByNameShort(name);
            if (!locale.equals("en")) {

                Map<String, String> localeMap = author.getLocalisation().get(locale);

                m.addAttribute("localName", localeMap.get("nameShort"));
                m.addAttribute("localFullName", localeMap.get("nameFull"));
                m.addAttribute("country", localeMap.get("country"));
                m.addAttribute("infoUrl", localeMap.get("infoUrl"));
            }
            m.addAttribute("author", author);
            m.addAttribute("locales", author.getLocalisation().keySet());
            m.addAttribute("this_locale", locale);
            return "authors/details_author";

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get author info");
        }
    }

    /**
     * This handlers supports as adding author as also updating it
     */
    @RequestMapping("/create/author")
    public String createAuthor(Model m,
            @RequestParam(value = "name", required = false) String name) {

        try {

            m.addAttribute("author", name == null ? new Author() : authorsRepo.findByNameShort(name));
            return "authors/create_author";

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't connect to DB");
        }
    }

    @RequestMapping(value = "/create/author", method = RequestMethod.POST)
    public String saveAuthor(Model m, Principal p,
                             @ModelAttribute Author author) {

        try {
            author.setLastChangedBy(p.getName());
            author.setLastChangedTimeStamp(new Date());

            Author existedAuthor = authorsRepo.findByNameShort(author.getNameShort());

            if (existedAuthor != null) {
                // Keeping previous Author info (locale etc.)
                author.setDocumentCreator(existedAuthor.getDocumentCreator());
                author.setCreatedTimeStamp(existedAuthor.getCreatedTimeStamp());
                author.setLocalisation(existedAuthor.getLocalisation());
            } else {
                author.setDocumentCreator(p.getName());
                author.setCreatedTimeStamp(new Date());
            }
            author = authorsRepo.save(author);
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't create author");
        }

        if (author != null) {
            m.addAttribute("author", author);
            return "redirect:/author/en?name=" + author.getNameShort();
        }
        m.addAttribute("error", "Can't create author");
        return "errors_page";
    }

    /**
     * This handlers for the Author localization
     */
    @RequestMapping("/create/author/localize")
    public String localizeAuthor(@RequestParam(value = "name") String name, Model m) {

        m.addAttribute("author", name);
        try {
            m.addAttribute("locales", MongoDBRequests.getLocales());
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't localize author");
        }

        return "authors/localize_author";
    }

    @RequestMapping(value = "/create/author/localize/{name}", method = RequestMethod.POST)
    public String saveLocalizedAuthor(Model m, Principal p
            , @PathVariable("name") String name
            , @RequestParam("locale") String locale
            , @RequestParam("nameShort") String nameShort
            , @RequestParam("nameFull") String nameFull
            , @RequestParam("country") String country
            , @RequestParam("infoUrl") String infoUrl
    ) {

        Author author;


        try {
            author = authorsRepo.findByNameShort(name);
            author.setLastChangedBy(p.getName());
            author.setLastChangedTimeStamp(new Date());
            author.localize(locale, nameShort, nameFull, country, infoUrl, p.getName());
            author = authorsRepo.save(author);

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't localize author");
        }

        if (author != null) {
            m.addAttribute("author", author);

            // This snippet updates all melodies with this author in the background

            final Author finalAuthor = author;
            new Thread() {
                @Override
                public void run() {
                    try {
                        List<Melody> melodies = melodiesRepo.findByAuthorNameShort(finalAuthor.getNameShort());

                        melodies.forEach(m -> {
                            m.setAuthor(finalAuthor);
                            melodiesRepo.save(m);
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            return "redirect:/author/" + locale + "?name=" + author.getNameShort();
        }
        m.addAttribute("error", "Can't localize author");
        return "errors_page";
    }

    /**
     * This handlers for removing the Author from DB
     */
    @RequestMapping(value = "/delete/author/{name}", method = RequestMethod.POST)
    public String deleteAuthor(@PathVariable("name") String name, Model m) {
        try {
            Author author = authorsRepo.findByNameShort(name);
            if (author != null) {
                authorsRepo.delete(author);
                return "redirect:/authors";
            }
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't delete author");
        }
        m.addAttribute("error", "Can't delete author");
        return "errors_page";
    }

}
