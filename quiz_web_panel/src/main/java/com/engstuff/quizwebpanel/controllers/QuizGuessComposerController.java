package com.engstuff.quizwebpanel.controllers;

import com.engstuff.quizwebpanel.data.DifficultyLevel;
import com.engstuff.quizwebpanel.data.mongodb.MongoDBRequests;
import com.engstuff.quizwebpanel.data.repositories.MelodiesRepo;
import com.engstuff.quizwebpanel.data.repositories.QuizGuessComposerRepo;
import com.engstuff.quizwebpanel.entities.Author;
import com.engstuff.quizwebpanel.entities.Melody;
import com.engstuff.quizwebpanel.entities.QuestionGuessComposer;
import com.engstuff.quizwebpanel.entities.QuizGuessComposer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.engstuff.quizwebpanel.controllers.Helpers.handleDbConnectionError;

@Controller
public class QuizGuessComposerController {

    @Autowired
    MelodiesRepo melodiesRepo;

    @Autowired
    QuizGuessComposerRepo quizRepo;

    /**
     * List of all user Quizzes
     */
    @RequestMapping("/quizzes")
    public String getAllQuizzes(Model m, Principal p) {

        try {
            List<QuizGuessComposer> qList = quizRepo.findByCreatedBy(p.getName());
            m.addAttribute("q_list", qList);
            return "quiz/quizzes";
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get list of quizzes from DB");
        }
    }

    /**
     * Detailed info about quiz (localized version included)
     */
    @RequestMapping("/quizzes/quiz/{locale}")
    public String quizDetails(Model m, Principal p,
                              @PathVariable("locale") String locale,
                              @RequestParam("_id") String _id) {

        try {
            QuizGuessComposer quiz = quizRepo.findById(_id);
            if (quiz == null) {
                m.addAttribute("error", "Can't get Quiz from DB");
                return "errors_page";
            }

            if (!locale.equals("en")) {

                Map<String, String> localeMap = quiz.getLocalisation().get(locale);

                m.addAttribute("localTitle", localeMap.get("title"));
            }

            m.addAttribute("locales", quiz.getLocalisation().keySet());
            m.addAttribute("this_locale", locale);
            m.addAttribute("quiz", quiz);
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get list of melodies from DB");
        }
        return "quiz/details_quiz";
    }

    /**
     * This 3 handlers supports as adding melody as also updating it
     */
    @RequestMapping("/create/quiz")
    public String createQuiz(Model m, Principal p,
                             @RequestParam("qnum") int qNum,
                             @RequestParam(value = "_id", required = false) String _id) {

        m.addAttribute("a_name", p.getName());
        m.addAttribute("q_num", qNum);
        if (_id != null) {
            try {
                m.addAttribute("title", quizRepo.findById(_id).getTitle());
                m.addAttribute("_id", _id);
            } catch (Exception e) {
                return handleDbConnectionError(m, e, "Can't get quiz from DB");
            }
        }
        return "quiz/create_quiz_guess_composer";
    }

    @RequestMapping("/create/quiz/new")
    public String addMelodiesToQuiz(Model m, Principal p,
                                    @RequestParam("title") String title,
                                    @RequestParam("qnum") int qNum,
                                    @RequestParam("level") DifficultyLevel level,
                                    @RequestParam(value = "_id", required = false) String _id) {

        try {
            m.addAttribute("principal_melodies", melodiesRepo.findByDocumentCreator(p.getName()));
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get list of melodies from DB");
        }
        m.addAttribute("qnum", qNum);
        m.addAttribute("level", level);
        m.addAttribute("title", title);
        m.addAttribute("a_name", p.getName());
        if (_id != null) m.addAttribute("_id", _id);

        return "quiz/choose_melodies";
    }

    @RequestMapping(value = "/create/quiz/melodies", method = RequestMethod.POST)
    public String addMelodiesToQuiz(Model m, Principal p,
                                    @RequestParam("melody") List<String> melodies,
                                    @RequestParam("title") String title,
                                    @RequestParam("qnum") int qNum,
                                    @RequestParam("level") DifficultyLevel level,
                                    @RequestParam(value = "_id", required = false) String _id) {

        if (melodies.size() != qNum) {
            m.addAttribute("error",
                    "Impossible to create Quiz. Number of melodies should be: "
                            + qNum + ", but we've got: " + melodies.size());
            return "errors_page";
        }

        QuizGuessComposer quiz = new QuizGuessComposer(p.getName(), title, qNum, level);

        if (_id != null) {
            quiz.set_id(_id);
            try {
                quiz.setLocalisation(quizRepo.findById(_id).getLocalisation());
            } catch (Exception e) {
                return handleDbConnectionError(m, e, "Can't keep localization");
            }
        }

        try {
            melodies.forEach(mld -> quiz
                    .addQuestion(new QuestionGuessComposer(melodiesRepo.findById(mld))));

            return "redirect:/quizzes/quiz/en?_id=" + quizRepo.save(quiz).get_id();

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't get melody from DB or save Quiz");
        }
    }

    /**
     * This handlers for the Quiz localization
     */
    @RequestMapping("/create/quiz/localize")
    public String localizeAuthor(Model m,
                                 @RequestParam("_id") String _id,
                                 @RequestParam("quiz_title") String quiz_title) {

        m.addAttribute("_id", _id);
        m.addAttribute("quiz_title", quiz_title);
        try {
            m.addAttribute("locales", MongoDBRequests.getLocales());
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't localize author");
        }

        return "quiz/localize_quiz";
    }

    @RequestMapping(value = "/create/quiz/localize/{_id}", method = RequestMethod.POST)
    public String saveLocalizedAuthor(Model m, Principal p
            , @PathVariable("_id") String _id
            , @RequestParam("locale") String locale
            , @RequestParam("title") String title
    ) {

        QuizGuessComposer quiz;


        try {
            quiz = quizRepo.findById(_id);
            quiz.localize(locale, title, p.getName());
            quiz = quizRepo.save(quiz);

        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't localize author");
        }

        if (quiz != null) {
            m.addAttribute("quiz", quiz);

            return "redirect:/quizzes/quiz/" + locale + "?_id=" + quiz.get_id();
        }
        m.addAttribute("error", "Can't localize author");
        return "errors_page";
    }

    /**
     * This handlers for removing the Quiz from DB
     */
    @RequestMapping(value = "/delete/quiz/{_id}", method = RequestMethod.POST)
    public String deleteMelody(@PathVariable("_id") String _id, Model m) {

        try {
            QuizGuessComposer quiz = quizRepo.findById(_id);
            if (quiz != null) {
                quizRepo.delete(quiz);
                return "redirect:/quizzes";
            }
        } catch (Exception e) {
            return handleDbConnectionError(m, e, "Can't delete melody");
        }
        m.addAttribute("error", "Can't delete melody");
        return "errors_page";
    }
}
