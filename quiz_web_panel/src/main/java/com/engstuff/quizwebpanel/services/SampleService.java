package com.engstuff.quizwebpanel.services;

public class SampleService {


    private String field;

    public String greetings() {
        return field;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}

