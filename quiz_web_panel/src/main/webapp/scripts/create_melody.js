function addMelodyClicked(event, types, ext) {

    if (!checkIfAuthorWasChosen(event) || !checkYear(event)) return;

    checkYear(event);

    checkMusicFile(event, types, ext);

}

function checkIfAuthorWasChosen(event) {
    var dropDown = document.getElementById("author");
    var selectedValue = dropDown.options[dropDown.selectedIndex].value;

    if (selectedValue == "none") {
        event.preventDefault();
        alert("Please choose an Author!");
        return false;
    }
    return true;
}

function checkYear(event) {
    var yearInput = document.getElementById("year").value;

    if(isNaN(yearInput) || yearInput < 1000 || yearInput > 2020) {
        event.preventDefault();
        alert("It seems that year input: \"" + yearInput + "\" is incorrect");
        return false;
    }
    return true;
}

function checkMusicFile(event, types, ext) {

    var input = document.getElementById("input_file");
    var file;

    if (!input.value) {
        event.preventDefault();
        alert("Please choose a music file!");
        return false;
    } else {
        file = input.files[0];
        if (file.size > 1048576) {
            event.preventDefault();
            alert("Too large file: " + (file.size / 1024 / 1024).toFixed(2) + "Mb");
            return false;
        } else if (types.split(':').indexOf(file.type) == -1) {
            event.preventDefault();
            alert("Wrong file type: " + file.type + "!");
            return false;
        } else if (ext.split(":").indexOf(file.name.split(".").pop().toLowerCase()) == -1) {
            event.preventDefault();
            alert("Wrong file extension. Allowed: " + ext);
            return false;
        }
    }
}

