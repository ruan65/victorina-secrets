package com.engstuff.classiccomposers.data;

import android.provider.BaseColumns;

public final class QuizDataContract {

    public static final String DB_NAME = "classical.db";
    public static final int DB_VERSION = 1;

    private QuizDataContract() {}

    public static final class ComposerEntry implements BaseColumns {

        public static final String TABLE_NAME = "composers";
        public static final String CN_NAME_SHORT = "name_short"; // CN = column name
        public static final String CN_NAME_FULL = "name_full";
        public static final String CN_COUNTRY = "country";
        public static final String CN_BIRTH_Y = "birth_year";
        public static final String CN_IMAGE_URI = "image_uri";
        public static final String CN_INFO_URL = "info_url";

        public static final String SQL_CREATE_TABLE_COMPOSERS =
                "CREATE TABLE " + TABLE_NAME + " ("

                        + _ID + " INTEGER PRIMARY KEY, "
                        + CN_NAME_SHORT + " TEXT NOT NULL, "
                        + CN_NAME_FULL + " TEXT, "
                        + CN_COUNTRY + " TEXT, "
                        + CN_BIRTH_Y + " INTEGER, "
                        + CN_IMAGE_URI + " TEXT, "
                        + CN_INFO_URL + " TEXT, "
                        + " UNIQUE (" + CN_NAME_SHORT + ") ON CONFLICT IGNORE"

                + ");";
    }

    public static final class MelodyEntry implements BaseColumns {

        public static final String TABLE_NAME = "melodies";
        public static final String CN_MELODY = "melody";
        public static final String CN_COMPOSER_ID = "composer_id";
        public static final String CN_MP3_URI = "mp3_uri";
        public static final String CN_INFO_URL = "info_url";
        public static final String CN_YEAR_CREATED = "year_created";

        public static final String SQL_CREATE_TABLE_MELODIES =
                "CREATE TABLE " + TABLE_NAME + " ("

                        + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + CN_MELODY + " TEXT NOT NULL, "
                        + CN_MP3_URI + " TEXT NOT NULL, "
                        + CN_INFO_URL + " TEXT, "
                        + CN_YEAR_CREATED + " INTEGER, "
                        + CN_COMPOSER_ID + " TEXT NOT NULL, "
                        + "FOREIGN KEY (" + CN_COMPOSER_ID + ") REFERENCES "
                        + ComposerEntry.TABLE_NAME + " (" + _ID + ")"
                        + " UNIQUE (" + CN_MP3_URI + ") ON CONFLICT IGNORE"

                + ");";
    }
}
