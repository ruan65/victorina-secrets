package com.engstuff.classiccomposers.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.engstuff.classiccomposers.data.QuizDataContract.*;

public class QuizDbHelper extends SQLiteOpenHelper {



    public QuizDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(ComposerEntry.SQL_CREATE_TABLE_COMPOSERS);

        sqLiteDatabase.execSQL(MelodyEntry.SQL_CREATE_TABLE_MELODIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MelodyEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ComposerEntry.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }
}
