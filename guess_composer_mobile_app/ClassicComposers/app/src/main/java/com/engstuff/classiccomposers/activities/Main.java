package com.engstuff.classiccomposers.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.engstuff.classiccomposers.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        ButterKnife.inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.main;
    }

//    public void onClickStartQuiz(View view) {
//        startActivity(new Intent(this, QuizActivity.class));
//    }
//
//    @OnClick(R.id.btnStartQuiz)
//    public void startQuiz() {
//        startActivity(new Intent(this, QuizActivity.class));
//    }
}
