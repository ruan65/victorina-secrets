package com.engstuff.classiccomposers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.engstuff.classiccomposers.data.QuizDataContract;
import com.engstuff.classiccomposers.data.QuizDbHelper;

import java.util.Map;
import java.util.Set;

public class TestDb extends AndroidTestCase {

    public void testCreateDb() {
        mContext.deleteDatabase(QuizDataContract.DB_NAME);
        SQLiteDatabase db = new QuizDbHelper(mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testInsertReadDb() {

        SQLiteDatabase db = new QuizDbHelper(mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        ContentValues cv = new ContentValues();
        cv.put("name_short", "Mozart");
        cv.put("name_full", "Volfgang Amadeus Mozart");

        long rowId = db.insert(QuizDataContract.ComposerEntry.TABLE_NAME, null, cv);

        assertTrue(rowId != -1);

        Cursor cursor = db.rawQuery("select name_short, name_full from composers;", null);

        validateCursor(cv, cursor);
    }

    static public void validateCursor(ContentValues expectedValues, Cursor cursor) {

        assertTrue(cursor.moveToFirst());
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {

            String columnName = entry.getKey();
            int indx = cursor.getColumnIndex(columnName);

            assert indx != -1;
            String expectedValue = entry.getValue().toString();

            Log.d("ml", cursor.getString(indx));

            assertEquals(expectedValue, cursor.getString(indx));
        }
    }
}
